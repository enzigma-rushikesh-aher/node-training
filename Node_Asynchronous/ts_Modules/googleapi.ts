import got from 'got';


export async function getLocation( address: string){
 try {
   const req:string = 'http://www.mapquestapi.com/geocoding/v1/address?key=Ylq1geAL9HKLeH5jD0Nsd5AHKXtoe7Pp&location='+address;
    const response = await got(req);
    let latitude = JSON.parse(response.body);

    return JSON.stringify(latitude.results[0].locations[0].displayLatLng); 
  } catch (error) {
    return error;
  }
}
