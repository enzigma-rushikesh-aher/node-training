export default interface training{
    traineeName:string,
    duration: string,
    moduleList: Array<String>,
    trainingStatus(traningObject:training),
    noOfModules: number,
}

