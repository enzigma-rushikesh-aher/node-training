import { expect } from 'chai';
import StringClass from '../allmodules/stringHandler';


describe('String Handler name string to array',()=>{
    it('should have 4 length',()=>{
        let result:string[]=StringClass.nameHandler('I am member of Enzigma');
        expect(result).to.be.lengthOf(5);
    })

    it('should return Empty string',()=>{
        let result:any=StringClass.nameHandler('');
        expect(result).to.equal('Empty string');
    })
})
