import { expect } from 'chai'
import ArrayClass from '../allmodules/arrayHandler';

describe('array handler ',()=>{
    // const arrayObject = new arrayClass();
    describe('new traniees',()=>{

        it('should insert traninee',()=>{
           let result = ArrayClass.newTrainees('Rushi');
           expect(result).to.have.lengthOf(1);
        })

        // it('should return empty array',()=>{
        //     let result:string[] = ArrayClass.newTrainees("");
        //     expect(result).to.have.lengthOf(1);
        //  })
    })

    describe('no Of Trainees',()=>{
        it('should return 0 as no elements are present',()=>{
            let result:number = ArrayClass.noOfTrainees();
            expect(result).to.equal(1);
        })
    })

    describe('add At Top',()=>{
        it('should return element at top',()=>{
            let result:string[] = ArrayClass.addAtTop('Alok');
            expect(result[0]).to.equal('Alok');
        })

        it('should not add duplicate element on top',()=>{
            let result:string = ArrayClass.addAtTop('Alok');
            expect(result).to.equal('Trainee already exist');
        })
    })

    describe('add trainee',()=>{
        it('should add trainee ',()=>{
            let result:string[]=ArrayClass.addTrainee('Swapnil');
            expect(result).to.be.lengthOf(3);
        })

        it('should add trainee at end',()=>{
            let result:string[]=ArrayClass.addTrainee('Swapnil');
            expect(result[(result.length)-1]).to.equal('Swapnil');
        })
    })

    describe('remove trainee',()=>{
        before(()=>{
            ArrayClass.addTrainee('Ashutosh');
        })

        it('remove elements of 2nd index',()=>{
            let result:string[]=ArrayClass.removeTrainee();
            expect(result).to.be.lengthOf(3);
        })
    })

    describe('sort array',()=>{
        it('sorting with ascending order',()=>{
            let result:string[]=ArrayClass.sortTrainee();
            expect(result[0]).to.equal('Alok');
            expect(result[1]).to.equal('Ashutosh');
            expect(result[2]).to.equal('Rushi');
        })
    })
})