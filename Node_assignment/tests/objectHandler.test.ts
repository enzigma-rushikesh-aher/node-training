import  ObjectClass from '../allmodules/objectHandler';
import { expect } from 'chai';
import training from '../allmodules/training';

describe('object Handler class',()=>{
    let obj:any= new ObjectClass();

    it('start training with no modules completed',(done)=>{
        let result:any = obj.startTraining('Rushikesh');
        expect(result.traineeName).to.equal('Rushikesh');
        done();
    })


    // it('start training with some modules completed',(done)=>{
    //     let result:any = obj.startTraining('Rushikesh');
    //     expect(result).to.be.lengthOf(1);
    //     done();
    // })

    // it('start training with all modules completed',(done)=>{
    //     let result:any = obj.startTraining('Rushikesh');
    //     expect(result).to.equal('Training Done Successfully with Modules');
    //     done();
    // })
    
})